![simpledft logo](https://gitlab.com/wangenau/simpledft_pages/-/raw/main/docs/_static/logo/simpledft_logo.png)

# [SimpleDFT pages](https://wangenau.gitlab.io/simpledft_pages)
[![License](https://img.shields.io/badge/license-Apache2.0-1a962b?logo=python&logoColor=a0dba2&label=License)](https://gitlab.com/wangenau/simpledft_pages/-/blob/main/LICENSE)

SimpleDFT is a simple plane wave density functional theory (DFT) code.
It is an implementation of the [DFT++](https://arxiv.org/abs/cond-mat/9909130) pragmas proposed by Thomas Arias et al.
Also, it serves as the minimalistic prototype for the [eminus](https://gitlab.com/wangenau/eminus) code, which was introduced in the [master thesis](https://www.researchgate.net/publication/356537762_Domain-averaged_Fermi_holes_A_self-interaction_correction_perspective) of Wanja Timm Schulze to explain theory and software development compactly.
There exists one version written in Python, called [SimpleDFT](https://gitlab.com/wangenau/simpledft), and one version written in Julia, called [SimpleDFT.jl](https://gitlab.com/wangenau/simpledft.jl).
This repository builds a documentation for both codes to compare the implementation from Python to Julia.

## Citation

A supplementary paper for [eminus](https://gitlab.com/wangenau/eminus) is available on [arXiv](https://arxiv.org/abs/2410.19438). The following BibTeX key can be used

```terminal
@Misc{Schulze2021,
  author    = {Schulze, Wanja T. and Schwalbe, Sebastian and Trepte, Kai and Gr{\"a}fe, Stefanie},
  title     = {eminus -- Pythonic electronic structure theory},
  year      = {2024},
  doi       = {10.48550/arXiv.2410.19438},
  publisher = {arXiv},
}
```
