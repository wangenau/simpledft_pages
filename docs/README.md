# docs

[Sphinx](https://www.sphinx-doc.org) documentation pages using the [Furo](https://pradyunsg.me/furo).

Additionally, it uses the Sphinx extension [remoteliteralinclude](https://github.com/wpilibsuite/sphinxext-remoteliteralinclude).

All packages can be installed with

```terminal
pip install sphinx furo sphinxext-remoteliteralinclude
```

The document can be built to the public folder using

```terminal
sphinx-build -b html ./docs ./public
```

