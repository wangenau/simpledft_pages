.. _xc:

:hide-toc:

xc
**

lda_slater_x
============

Slater exchange functional (spin-paired).

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/xc.py
          :language: python
          :lines: 8-13
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/xc.jl
          :language: julia
          :lines: 6-11
          :linenos:

lda_chachiyo_c
==============

Chachiyo parametrization of the correlation functional (spin-paired).

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/xc.py
          :language: python
          :lines: 18-25
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/xc.jl
          :language: julia
          :lines: 17-24
          :linenos:
