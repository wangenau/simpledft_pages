.. _operators:

:hide-toc:

operators
*********

O
=

Overlap operator.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 32,36
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 5-7
          :linenos:

L
=

Laplacian operator.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 39,44-48
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 15-22
          :linenos:

Linv
====

Inverse Laplacian operator.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 51,55-64
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 29-33,40-44
          :linenos:

I
=

Backwards transformation from reciprocal space to real-space.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 67,72-88
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 52-63,71-77
          :linenos:

J
=

Forward transformation from real-space to reciprocal space.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 91,96-103
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 85-90,98-104
          :linenos:

Idag
====

Conjugated backwards transformation from real-space to reciprocal space.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 106,111-114
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 112-117,125-131
          :linenos:

Jdag
====

Conjugated forward transformation from reciprocal space to real-space.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/operators.py
          :language: python
          :lines: 117,122-124
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/operators.jl
          :language: julia
          :lines: 139-143,151-155
          :linenos:
