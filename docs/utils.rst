.. _utils:

:hide-toc:

utils
*****

pseudo_uniform
==============

Lehmer random number generator, follwoing MINSTD.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/utils.py
          :language: python
          :lines: 12-20
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/utils.jl
          :language: julia
          :lines: 3-13
          :linenos:
