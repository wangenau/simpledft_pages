.. _atoms:

:hide-toc:

atoms
*****

get_index_matrices
==================

Build index matrices M and N to build the real and reciprocal space samplings.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/atoms.py
          :language: python
          :lines: 28-37
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/atoms.jl
          :language: julia
          :lines: 37-46
          :linenos:

set_cell
========

Build the unit cell and create the respective sampling.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/atoms.py
          :language: python
          :lines: 47-48,51-52,54-55
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/atoms.jl
          :language: julia
          :lines: 59-64
          :linenos:

set_G
=====

Build G-vectors, build squared magnitudes G2, and generate the active space.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/atoms.py
          :language: python
          :lines: 66,68,70,72-73
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/atoms.jl
          :language: julia
          :lines: 79-83
          :linenos:
