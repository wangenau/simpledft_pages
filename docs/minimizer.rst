.. _minimizer:

:hide-toc:

minimizer
*********

sd
==

Steepest descent minimization algorithm.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/minimizer.py
          :language: python
          :lines: 22-34
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/minimizer.jl
          :language: julia
          :lines: 19-33
          :linenos:
