.. _index:

Home
****

.. meta::
   :description: Documentation website for the SimpleDFT code.
   :author: Wanja Timm Schulze

.. image:: https://img.shields.io/badge/3-Python?color=1a962b&logo=python&logoColor=a0dba2&label=Python
   :target: https://www.python.org

.. image:: https://img.shields.io/badge/1.10-Julia?color=1a962b&logo=julia&logoColor=a0dba2&label=Julia
   :target: https://www.julialang.org

.. image:: https://img.shields.io/badge/license-Apache2.0-1a962b?logo=python&logoColor=a0dba2&label=License
   :target: https://gitlab.com/wangenau/simpledft_pages/-/blob/main/LICENSE

| SimpleDFT is a simple plane wave density functional theory (DFT) code.
| It is an implementation of the `DFT++ <https://arxiv.org/abs/cond-mat/9909130>`_ pragmas proposed by Thomas Arias et al.
| Also, it serves as the minimalistic prototype for the `eminus <https://gitlab.com/wangenau/eminus>`_ code,
| which was introduced in the `master thesis <https://www.researchgate.net/publication/356537762_Domain-averaged_Fermi_holes_A_self-interaction_correction_perspective>`_ of Wanja Timm Schulze to explain theory and software development compactly.
| There exists one version written in Python, called `SimpleDFT <https://gitlab.com/wangenau/simpledft>`_, and one version written in Julia, called `SimpleDFT.jl <https://gitlab.com/wangenau/simpledft.jl>`_.
| The purpose of this website is to compare the implementation from Python to Julia.
| The following pages contain side-by-side comparisons of the most functions for each module.
| The comparison for a simple example would look like the following

.. list-table::
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. code-block:: python
          :linenos:

          from simpledft import Atoms, SCF

          atoms = Atoms(["He"], [0, 0, 0],
                        16, 16, [2], [60, 60, 60], [2])
          etot = SCF(atoms).run()
          print("Etot({}) = {:.6f} Eh".format(atoms.atom, etot))
          # Output:  Etot(["He"]) = -2.632035 Eh
     - .. code-block:: julia
          :linenos:

          using SimpleDFT

          atoms = Atoms(["He"], [0.0 0.0 0.0;],
                        16.0, 16.0, [2.0], [60, 60, 60], [2.0])
          etot = runSCF(atoms)
          println("Etot($(atoms.atom)) = $(round(etot; digits=6)) Eh")
          # Output:  Etot(["He"]) = -2.632035 Eh

.. toctree::
   :caption: Contents
   :maxdepth: 1
   :numbered:
   :hidden:

   self
   atoms.rst
   dft.rst
   energies.rst
   minimizer.rst
   operators.rst
   potentials.rst
   utils.rst
   xc.rst
