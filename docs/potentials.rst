.. _potentials:

:hide-toc:

potentials
**********

coulomb
=======

All-electron Coulomb potential.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/potentials.py
          :language: python
          :lines: 9-12
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/potentials.jl
          :language: julia
          :lines: 7-9
          :linenos:
