.. _energies:

:hide-toc:

energies
********

get_Ekin
========

Calculate the kinetic energy.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/energies.py
          :language: python
          :lines: 21-23
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/energies.jl
          :language: julia
          :lines: 19-21
          :linenos:

get_Ecoul
=========

Calculate the Coulomb energy.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/energies.py
          :language: python
          :lines: 30-31
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/energies.jl
          :language: julia
          :lines: 30-31
          :linenos:

get_Exc
=======

Calculate the exchange-correlation energy.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/energies.py
          :language: python
          :lines: 38-39
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/energies.jl
          :language: julia
          :lines: 40-41
          :linenos:

get_Een
=======

Calculate the electron-ion interaction.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/energies.py
          :language: python
          :lines: 46-47
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/energies.jl
          :language: julia
          :lines: 50-51
          :linenos:
