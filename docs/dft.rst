.. _dft:

:hide-toc:

dft
***

solve_poisson
=============

Solve the Poisson equation.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 10
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 6
          :linenos:

get_n_total
===========

Calculate the total electronic density.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 18-20
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 16-18
          :linenos:

orth
====

Orthogonalize coefficient matrix W.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 27-28
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 27-28
          :linenos:

get_grad
========

Calculate the energy gradient with respect to W.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 36-44
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 38-46
          :linenos:

H
=

Left-hand side of the eigenvalue equation.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 52-53
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 56-57
          :linenos:

Q
=

Operator needed to calculate gradients with non-constant occupations.

.. list-table::
   :width: 95%
   :widths: 50 50
   :header-rows: 1

   * - Python
     - Julia
   * - .. rli:: https://gitlab.com/wangenau/simpledft/-/raw/main/simpledft/dft.py
          :language: python
          :lines: 61-65
          :linenos:
     - .. rli:: https://gitlab.com/wangenau/simpledft.jl/-/raw/main/src/dft.jl
          :language: julia
          :lines: 67-71
          :linenos:
